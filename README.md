# Packer and Salt LAMP AMI
A simple Packer definition to;

 - Take the latest Ubuntu 18.04 LTS image
 - Install PHP 7, MySQL, and Apache
 - Create an Apache VirtualHost for `nextamazing.site`

## Usage

 - Install Packer  ([https://www.packer.io/downloads.html](https://www.packer.io/downloads.html))
 - Clone this repo
 - Run `packer build template.json`

To avoid having to provide AWS secrets (or over-ride ones already set up fro AWS CLI);

 - Create `aws_vars.json`
 - Execute packer to use these variables `packer build template.json`

Read more @ https://fluffycloudsandlines.blog/creating-a-lamp-ami-using-packer-and-salt/
