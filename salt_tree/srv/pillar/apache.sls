apache:
    sites:
        nextamazing.site:
            enabled: True
            Directory:
                default: {AllowOverride: All, Options: All, Require: all granted}
            DocumentRoot: /www/html/nextamazing.site
            ServerName: nextamazing.site
